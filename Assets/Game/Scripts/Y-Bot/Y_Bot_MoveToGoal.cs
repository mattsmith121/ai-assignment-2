using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Y_Bot_MoveToGoal : Y_Bot_BaseState
{
	public float angularDampeningTime = 5.0f;
	public float deadZone = 10.0f;
	public float offMeshInbetweenRotation = 1.0f;

	private UnityAction onAnimationMoveCallback;
	private AnimationListener animationListener;

	public override void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener = owner.GetComponent<AnimationListener>();
		onAnimationMoveCallback = new UnityAction(OnAnimationMove);
		animationListener.AddAnimationMoveListener(onAnimationMoveCallback);
	}

	public override void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (Input.GetMouseButton(1))
		{
			yBotAnimator.SetTrigger("Crying");
		}

		if (agent.desiredVelocity != Vector3.zero)
		{
			float speed = Vector3.Project(agent.desiredVelocity, transform.forward).magnitude * agent.speed;
			yBotAnimator.SetFloat("Speed", speed);

			float angle = Vector3.Angle(transform.forward, agent.desiredVelocity);
			if (Mathf.Abs(angle) <= deadZone)
			{
				transform.LookAt(transform.position + agent.desiredVelocity);
			}
			else
			{
				transform.rotation = Quaternion.Lerp(transform.rotation,
													 Quaternion.LookRotation(agent.desiredVelocity),
													 Time.deltaTime * angularDampeningTime);
			}
		}
		else
		{
			yBotAnimator.SetFloat("Speed", 0.0f);
			fsm.SetBool("MoveToGoal", false);
		}
	}

	public override void OnStateExit(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);
	}

	private void OnAnimationMove()
	{
		agent.velocity = yBotAnimator.deltaPosition / Time.deltaTime;
	}
}
