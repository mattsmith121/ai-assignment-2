using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Y_Bot_ClimbDownState : Y_Bot_BaseState
{
	private Animator fsmAnimator;

	private AnimationListener animationListener;
	private UnityAction onAnimationMoveCallback;
	private UnityAction<int> onAnimationCompletedCallback;

	private readonly int ANIM_ClimbingDown = Animator.StringToHash("ClimbingDown");

	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);

		animationListener = owner.GetComponent<AnimationListener>();
		onAnimationMoveCallback = new UnityAction(OnAnimationMove);
		onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
	}

	private void OnAnimationMove()
	{
		transform.position = yBotAnimator.rootPosition;
		// May not need if you are going to rotate early or use a specific direction
		// OR you can try and Lerp here too
		transform.rotation = yBotAnimator.rootRotation;
	}

	private void OnAnimationCompleted(int animationName)
	{
		agent.CompleteOffMeshLink();
		fsmAnimator.SetBool("ClimbDown", false);
	}

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		fsmAnimator = animator;

		animationListener.AddAnimationMoveListener(onAnimationMoveCallback);
		animationListener.AddAnimationCompletedListener(ANIM_ClimbingDown, onAnimationCompletedCallback);
	}

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);
		animationListener.RemoveAnimationCompletedListener(ANIM_ClimbingDown, onAnimationCompletedCallback);
	}
}
