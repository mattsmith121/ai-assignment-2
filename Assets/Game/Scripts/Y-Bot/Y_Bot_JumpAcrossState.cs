using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Y_Bot_JumpAcrossState : Y_Bot_BaseState
{
	private Animator fsmAnimator;

	private AnimationListener animationListener;
	private UnityAction onAnimatorMoveCallback;
	private UnityAction<int> onAnimationCompletedCallback;

	private readonly int ANIM_JumpAcross = Animator.StringToHash("JumpAcross");

	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);

		animationListener = owner.GetComponent<AnimationListener>();
		onAnimatorMoveCallback = new UnityAction(OnAnimatorMove);
		onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
	}

	private void OnAnimatorMove()
	{
		transform.position = yBotAnimator.rootPosition;
	}

	private void OnAnimationCompleted(int animationName)
	{
		agent.CompleteOffMeshLink();
		fsmAnimator.SetBool("JumpAcross", false);
	}

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		fsmAnimator = animator;

		animationListener.AddAnimationMoveListener(onAnimatorMoveCallback);
		animationListener.AddAnimationCompletedListener(ANIM_JumpAcross, onAnimationCompletedCallback);
	}

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.RemoveAnimationMoveListener(onAnimatorMoveCallback);
		animationListener.RemoveAnimationCompletedListener(ANIM_JumpAcross, onAnimationCompletedCallback);
	}
}
