using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Y_Bot_BaseState : FSMBaseState
{
	protected Y_Bot_Controller controller;
	protected NavMeshAgent agent;
	protected Transform transform;
	protected Animator yBotAnimator;

	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);

		controller = owner.GetComponent<Y_Bot_Controller>();
		Debug.Assert(controller != null, $"{owner.name} requires a Y_Bot_Controller Component");

		agent = owner.GetComponent<NavMeshAgent>();
		Debug.Assert(agent != null, $"{owner.name} requires a NavMeshAgent Component");

		transform = owner.transform;

		yBotAnimator = owner.GetComponent<Animator>();
		Debug.Assert(yBotAnimator != null, $"{owner.name} requires an Animator Component");
	}
}
