using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Y_Bot_JumpWithStyleState : Y_Bot_BaseState
{
	public enum JumpStates
	{
		JumpStart,
		JumpFlight,
		JumpLanding,
		JumpCompleted
	};
	public JumpStates jumpState = JumpStates.JumpStart;

	public float jumpHeight = 1.25f;
	public float jumpTime = 1.0f;

	// These should be private
	public float currentTime = 0.0f;
	public Vector3 startPosition;
	public Vector3 endPosition;

	private AnimationListener animationListener;
	private UnityAction<int> onAnimationCompletedCallback;

	private int ANIM_JumpStart = Animator.StringToHash("Jump-Start");
	private int ANIM_JumpLand = Animator.StringToHash("Jump-Land");

	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);

		animationListener = owner.GetComponent<AnimationListener>();
		onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
	}

	private void OnAnimationCompleted(int animationName)
	{
		if (animationName == ANIM_JumpStart)
		{
			jumpState = JumpStates.JumpFlight;
			startPosition = transform.position;
		}
		else if (animationName == ANIM_JumpLand)
		{
			jumpState = JumpStates.JumpCompleted;
		}
	}

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.AddAnimationCompletedListener(ANIM_JumpStart, onAnimationCompletedCallback);
		animationListener.AddAnimationCompletedListener(ANIM_JumpLand, onAnimationCompletedCallback);

		jumpState = JumpStates.JumpStart;

		endPosition = agent.currentOffMeshLinkData.endPos;
		currentTime = 0.0f;
	}

	public override void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		switch(jumpState)
		{
			case JumpStates.JumpStart:
				break;

			case JumpStates.JumpFlight:
				currentTime += Time.deltaTime;
				if (currentTime <= 1.0f)
				{
					float normalizedTime = currentTime / jumpTime;
					float yOffset = jumpHeight * 4.0f * (normalizedTime - normalizedTime * normalizedTime);
					transform.position = Vector3.Lerp(startPosition, endPosition, normalizedTime) + yOffset * Vector3.up;
				}
				else
				{
					yBotAnimator.SetTrigger("Jump-Land");
					jumpState = JumpStates.JumpLanding;
				}
				break;

			case JumpStates.JumpLanding:
				break;

			case JumpStates.JumpCompleted:
				agent.CompleteOffMeshLink();
				fsm.SetBool("Jump", false);
				break;
		}
	}

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.RemoveAnimationCompletedListener(ANIM_JumpStart, onAnimationCompletedCallback);
		animationListener.RemoveAnimationCompletedListener(ANIM_JumpLand, onAnimationCompletedCallback);
	}
}
