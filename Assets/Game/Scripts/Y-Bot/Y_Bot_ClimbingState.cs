using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Y_Bot_ClimbingState : Y_Bot_BaseState
{
	private Animator fsmAnimator;
	private AnimationListener animationListener;
	private UnityAction onAnimationMoveCallback;
	private UnityAction<int> onAnimationCompletedCallback;

	int ANIM_Climbing = Animator.StringToHash("Climbing");

	private void OnAnimatorMove()
	{
		transform.position = yBotAnimator.rootPosition;
	}

	private void OnAnimationCompleted(int animationName)
	{
		agent.CompleteOffMeshLink();
		fsmAnimator.SetBool("Climb", false);
	}

	public override void Init(GameObject _owner, FSM _fsm)
	{
		base.Init(_owner, _fsm);

		animationListener = owner.GetComponent<AnimationListener>();
		onAnimationMoveCallback = new UnityAction(OnAnimatorMove);
		onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
	}

	public override void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		fsmAnimator = fsm;
		animationListener.AddAnimationMoveListener(onAnimationMoveCallback);
		animationListener.AddAnimationCompletedListener(ANIM_Climbing, onAnimationCompletedCallback);
	}

	public override void OnStateExit(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);
		animationListener.RemoveAnimationCompletedListener(ANIM_Climbing, onAnimationCompletedCallback);
	}
}
