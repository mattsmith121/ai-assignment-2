using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Y_Bot_Controller : MonoBehaviour
{
    [HideInInspector] public NavMeshPath path;

    public bool drawPath = true;

    void Start()
    {
        path = new NavMeshPath();
    }

    private void OnDrawGizmos()
    {
        if (drawPath && path != null && path.corners.Length > 0)
        {
            Color drawColor = Color.yellow;
            if (path.status == NavMeshPathStatus.PathComplete)
            {
                drawColor = Color.green;
            }
            else if (path.status == NavMeshPathStatus.PathInvalid)
            {
                drawColor = Color.red;
            }

            for (int i = 1; i < path.corners.Length; i++)
            {
                Debug.DrawLine(path.corners[i - 1], path.corners[i]);
            }
        }
    }
}
