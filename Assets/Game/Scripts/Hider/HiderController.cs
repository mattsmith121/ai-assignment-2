using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
public class HiderController : MonoBehaviour
{
    public GameObject WaypointParent;
    public float distanceToWaypoint = 0.5f;
    public float angularDampeningTime = 5.0f;
    public float deadZone = 10.0f;
    public float cryTime = 5.0f;

    private int index = 0;
    private List<GameObject> waypoints = new List<GameObject>();
    private NavMeshAgent agent;
    private Animator animator;
    private float currentCryTime = 0.0f;

    // Cached hiding spot information
    private Vector3 lookDirection;
    private float xMod, zMod;

    private enum States
	{
        Walk,
        Hide,
        Cry
	};
    private States state = States.Walk;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        if (WaypointParent != null)
		{
            foreach(Transform waypoint in WaypointParent.transform)
			{
                waypoints.Add(waypoint.gameObject);
			}
            GetNewIndex();
        }

        GameManager.Instance.OnFound.AddListener(Found);
    }

	private void OnAnimatorMove()
	{
        switch (state)
        {
            case States.Walk:
                agent.velocity = animator.deltaPosition / Time.deltaTime;
                break;

            case States.Cry:
                float angleInDegrees;
                Vector3 rotationAxis;
                animator.deltaRotation.ToAngleAxis(out angleInDegrees, out rotationAxis);
                Vector3 angularDisplacement = rotationAxis * angleInDegrees * Mathf.Deg2Rad;
                agent.velocity = angularDisplacement / Time.deltaTime;
                break;

            case States.Hide:
                agent.velocity = new Vector3(animator.deltaPosition.x * xMod, animator.deltaPosition.y, animator.deltaPosition.z * zMod) / Time.deltaTime;
                break;
        }
    }

    void Update()
    {
        switch(state)
		{
            case States.Walk:
                Walk();
                break;

            case States.Hide:
                Hide();
                break;

            case States.Cry:
                Cry();
                break;
		}
    }

    private void Found() {
        state = States.Cry;
        currentCryTime = cryTime;
        animator.SetBool("Hide", false);
        animator.SetBool("Cry", true);
        agent.isStopped = true;
    }

    private void Walk()
	{
		float distance = (waypoints[index].transform.position - transform.position).magnitude;
		if (distance < distanceToWaypoint || agent.isStopped || agent.desiredVelocity == Vector3.zero)
		{
            // Reached destination (hiding spot) switch to Hide state
            state = States.Hide;
            animator.SetBool("Hide", true);
            agent.isStopped = true;
            return;
        }

		float speed = Vector3.Project(agent.desiredVelocity, transform.forward).magnitude * agent.speed;
		animator.SetFloat("Speed", speed);

		float angle = Vector3.Angle(transform.forward, agent.desiredVelocity);
		if (Mathf.Abs(angle) <= deadZone)
		{
			transform.LookAt(transform.position + agent.desiredVelocity);
		}
		else
		{
			transform.rotation = Quaternion.Lerp(transform.rotation,
												 Quaternion.LookRotation(agent.desiredVelocity),
												 Time.deltaTime * angularDampeningTime);
		}
    }

    private void Cry()
	{
        currentCryTime -= Time.deltaTime;
        if (currentCryTime <= 0.0f)
		{
            state = States.Walk;
            animator.SetBool("Cry", false);

            // Get next location
            GetNewIndex();
            agent.isStopped = false;
        }
    }

    private void Hide() {
        transform.rotation = Quaternion.Lerp(transform.rotation,
                                 Quaternion.LookRotation(lookDirection),
                                 Time.deltaTime * angularDampeningTime);
    }

    private void GetNewIndex() {
        int oldIndex = index;
        int tries = 0; // to break any infinite loops (won't happen if waypoints > 1)
        while (oldIndex == index && tries <= 20) {
            index = Random.Range(0, waypoints.Count);
            tries++;
        }
        xMod = waypoints[index].GetComponent<HidingSpot>().noXMovement ? 0.1f : 1f;
        zMod = waypoints[index].GetComponent<HidingSpot>().noZMovement ? 0.1f : 1f;
        lookDirection = waypoints[index].GetComponent<HidingSpot>().LookDirection();
        agent.SetDestination(waypoints[index].transform.position);

    }
}
