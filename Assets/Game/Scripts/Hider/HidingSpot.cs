using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingSpot : MonoBehaviour
{
    public Transform lookDirection;
    [Tooltip("Negate animation movement along X axis.")]
    public bool noXMovement;
    [Tooltip("Negate animation movement along Z axis.")]
    public bool noZMovement;

    public Vector3 LookDirection() {
        return Vector3.Normalize(lookDirection.position - transform.position);
    }
}
